package xyz.zhangyi.diamond.demo.notificationcontext.message.north;


import xyz.zhangyi.diamond.demo.foundation.model.pl.ApplicationEvent;
import xyz.zhangyi.diamond.demo.notificationcontext.domain.Notification;

import java.io.Serializable;

public class OrderPlaced extends ApplicationEvent implements Serializable {
    private static final long serialVersionUID = 2907209232038297401L;

    public Notification to() {
        return null;
    }
}
