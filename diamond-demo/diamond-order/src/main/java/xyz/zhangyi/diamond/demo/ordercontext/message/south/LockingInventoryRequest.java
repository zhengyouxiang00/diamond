package xyz.zhangyi.diamond.demo.ordercontext.message.south;

import xyz.zhangyi.diamond.demo.foundation.stereotype.Direction;
import xyz.zhangyi.diamond.demo.foundation.stereotype.MessageContract;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;

import java.io.Serializable;

@MessageContract(Direction.South)
public class LockingInventoryRequest implements Serializable {
    private static final long serialVersionUID = 1482751979265813792L;

    public static LockingInventoryRequest from(Order order) {
        return null;
    }
}
