package xyz.zhangyi.diamond.demo.ordercontext.message.south;

import java.io.Serializable;

import xyz.zhangyi.diamond.demo.foundation.stereotype.Direction;
import xyz.zhangyi.diamond.demo.foundation.stereotype.MessageContract;
import xyz.zhangyi.diamond.demo.ordercontext.domain.InventoryReview;

@MessageContract(Direction.South)
public class InventoryReviewResponse implements Serializable {
    private static final long serialVersionUID = 6198611700664779114L;

    public static InventoryReviewResponse from(InventoryReview inventoryReview) {
        return null;
    }
    
    public InventoryReview to() {
        return null;
    }
}
