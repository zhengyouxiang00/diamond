package xyz.zhangyi.diamond.demo.ordercontext.message.south;

import java.io.Serializable;

import xyz.zhangyi.diamond.demo.foundation.stereotype.Direction;
import xyz.zhangyi.diamond.demo.foundation.stereotype.MessageContract;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;

@MessageContract(Direction.South)
public class CheckingInventoryRequest implements Serializable {
    private static final long serialVersionUID = 1185197568933571125L;

    public static CheckingInventoryRequest from(Order order) {
        return null;
    }
}
