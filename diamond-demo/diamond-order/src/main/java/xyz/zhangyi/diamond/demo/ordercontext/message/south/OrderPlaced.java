package xyz.zhangyi.diamond.demo.ordercontext.message.south;

import xyz.zhangyi.diamond.demo.foundation.model.pl.ApplicationEvent;
import xyz.zhangyi.diamond.demo.foundation.stereotype.Direction;
import xyz.zhangyi.diamond.demo.foundation.stereotype.MessageContract;
import xyz.zhangyi.diamond.demo.ordercontext.domain.order.Order;

import java.io.Serializable;

@MessageContract(Direction.North)
public class OrderPlaced extends ApplicationEvent implements Serializable {
    private static final long serialVersionUID = -5311497180696002181L;

    public static OrderPlaced from(Order order) {
        return new OrderPlaced();
    }
}
